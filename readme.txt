How to run:
- Go to directory of desired language (e.g. java, scala, groovy)
- For java:
	- javac ray.java
	- java ray 8 512
- For scala:
	- scalac ray.scala
	- scala scalaRay 8 512
- For groovy:
	- groovy ray.groovy 8 512
